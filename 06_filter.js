const fs = require('fs');
const path = require('path');

module.exports = function (dir, extension, callback) {
  extension = '.' + extension;
  
  fs.readdir(dir, function filter(err, files) {
    if(err) {
      return callback(err);
    }

    var filteredFiles = files.filter(function hasExtension(filename) {
      return path.extname(filename) === extension;
    });

    callback(null, filteredFiles);
  });
}
