var http = require('http');

var url = process.argv[2];
var data = '';

http.get(url, function (response) {
  response.setEncoding('utf8');
  response.on('data', append);
  response.on('end', outputResult);
  response.on('error', console.error);
});

function append(d) {
  data += d;
}

function outputResult() {
  console.log(data.length);
  console.log(data);
}
