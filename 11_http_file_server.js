const fs = require('fs');
const http = require('http');

var server = http.createServer(handleRequest);
server.listen(Number(process.argv[2]));

function handleRequest(request, response) {
  response.writeHead(200, { 'Content-Type': 'text/plain' });

  stream = fs.createReadStream(process.argv[3]);
  stream.pipe(response);
}
