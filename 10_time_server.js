const net = require('net');
const strftime = require('strftime');

var server = net.createServer(handleSocket);
server.listen(Number(process.argv[2]));

function handleSocket(socket) {
  socket.write(strftime('%F %R') + '\n');
  socket.end();
}
