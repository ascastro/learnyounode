var fs = require('fs');
var path = require('path');

var dir = process.argv[2];
var extension = '.' + process.argv[3];

fs.readdir(dir, function filter(err, files) {
  if(err) {
    console.log(err);
    return;
  }

  files.filter(function hasExtension(filename) {
    return path.extname(filename) === extension;
  }).forEach(function logFile(file) {
    console.log(file);
  });
});
