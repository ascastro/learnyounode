const http = require('http');

var server = http.createServer(handleRequest);
server.listen(Number(process.argv[2]));

function handleRequest(request, response) {
  if(request.method === 'POST') {
    response.writeHead(200, { 'Content-Type': 'text/plain' });

    request.on('data', function (chunk) {
      response.write(chunk.toString().toUpperCase());
    });

    request.on('end', function () {
      response.end();
    });
  } else {
    response.writeHead(501);
    response.end();
  }
}
