var http = require('http');

var responses = {};
var total = process.argv.length - 2;
for (i = 2; i < process.argv.length; i++) {
  var url = process.argv[i];
  responses[url] = '';
  makeRequest(url);
}

function makeRequest(url) {
  var data = '';

  http.get(url, function (response) {
    response.setEncoding('utf8');
    response.on('data', function (d) { data += d });
    response.on('end', function() { outputResult(url, data) });
    response.on('error', console.error);
  });
}

function outputResult(url, data) {
  responses[url] = data;
  total--;

  if(total > 0) {
    return;
  }

  for (i = 2; i < process.argv.length; i++) {
    console.log(responses[process.argv[i]]);
  }
}
