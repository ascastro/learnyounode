var fs = require('fs');

var filepath = process.argv[2];

fs.readFile(filepath, 'utf8', function countLines(err, content) {
  if(err) {
    return;
  }

  var matches = content.match(/\n/g);
  console.log(matches.length);
});
