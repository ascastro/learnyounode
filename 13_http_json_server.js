const http = require('http');
const url = require('url');

function handleGet(requestUrl) {
  var parsedUrl = url.parse(requestUrl, true);
  var date = new Date(parsedUrl.query['iso']);
  var result = null;

  if(parsedUrl.pathname === '/api/parsetime') {
    result = parsetime(date);
  } else if(parsedUrl.pathname === '/api/unixtime') {
    result = unixtime(date);
  }

  return result;
}

function parsetime(date) {
  return {
    "hour": date.getHours(),
    "minute": date.getMinutes(),
    "second": date.getSeconds()
  };
}

function unixtime(date) {
  return {
    "unixtime": date.getTime()
  };
}

function notImplemented(response) {
  response.writeHead(501);
  response.end();
}

var server = http.createServer(function (request, response) {
  if(request.method === 'GET') {
    var result = handleGet(request.url);

    if(result) {
      response.writeHead(200, { 'Content-Type': 'application/json' });
      response.end(JSON.stringify(result));
    }
  }

  // if we're here, the response wasn't closed, so method is not supported
  notImplemented(response);
}).listen(Number(process.argv[2]));
