const filter = require('./06_filter')

var dir = process.argv[2];
var extension = process.argv[3];

filter(dir, extension, function (err, files) {
  if(err) {
    console.error('An error occured:', err);
    return;
  }

  files.forEach(function logFile(file) {
    console.log(file);
  });
});
